<?php if(!defined('PLX_ROOT')) exit;
if (isset($_GET['print'])) :
	foreach ($this->plxRecord_adherents->result as $k => $v) {
		$this->plxRecord_adherents->result[$k]['nom'] = strtoupper($v['nom']);
		$this->plxRecord_adherents->result[$k]['prenom'] = ucfirst(strtolower($v['prenom']));
		$this->plxRecord_adherents->result[$k]['cotis'] = ($v['validation'] == 0) ? ' ' : strip_tags(str_replace('<br/>',' ',$this->cotisationAJour(intval($v['date']))));
		$this->plxRecord_adherents->result[$k]['choix'] = ($v['choix'] == 'adhesion') ? $this->getLang('L_ADHESION_OK') : ($v['choix'] == 'renouveler') ? $this->getLang('L_ADHESION_RENEW') : $this->getLang('L_ADHESION_STOP');
		$this->plxRecord_adherents->result[$k]['mailing'] = ($v['mailing'] == 'maillist') ? $this->getLang('L_NEWS_OK') : $this->getLang('L_NEWS_NO');
		$this->plxRecord_adherents->result[$k]['validation'] = ($v['validation'] == 1) ? $this->getLang('L_YES') : $this->getLang('L_NO');
		if ($this->getParam('showAnnuaire') == 'on') {
			$this->plxRecord_adherents->result[$k]['coordonnees'] = (($v['coordonnees'] == 'rec') ? $this->getLang('L_COORD_REC') : ($v['coordonnees'] == 'public') ? $this->getLang('L_COORD_PUBLIC') : $this->getLang('L_COORD_NO'));
		}
		else {
			$this->plxRecord_adherents->result[$k]['coordonnees'] = $this->getLang('L_DEACTIVATED');
		}
		if ($this->getParam('typeAnnuaire')=='generaliste') {
			$this->plxRecord_adherents->result[$k]['activite'] = '';
			$this->plxRecord_adherents->result[$k]['etablissement'] = '';
			$this->plxRecord_adherents->result[$k]['service'] = '';
			$this->plxRecord_adherents->result[$k]['tel_office'] = '';
		}
	}
endif;
ob_clean();#clean buffer before : Fix plxAdminBar-2.0.0 hook adminPrepend is in file buffer (& maybe others plugins)
//head lang
$l[]['titre'] = $this->getLang('L_PRINT_TITLE');//Liste des adhérents de l'association
$l[0]['inscrit'] = $this->getLang('L_INSCRIT');
$l[0]['cotis'] = $this->getLang('L_COTIS');
$l[0]['nom'] = $this->getLang('L_FORM_NAME');//L_ADMIN_LIST_NAME
$l[0]['prenom'] = $this->getLang('L_FORM_FIRST_NAME');//L_ADMIN_LIST_FIRST_NAME
$l[0]['courriel'] = $this->getLang('L_NOTI_MAIL');//L_ADMIN_LIST_MAIL
$l[0]['tel'] = $this->getLang('L_FORM_TEL');//L_ADMIN_LIST_TEL
$l[0]['adresse'] = $this->getLang('L_FORM_ADDRESS');//L_ADMIN_LIST_ADRESSE'
$l[0]['cp'] = $this->getLang('L_FORM_ZIP_CODE');//L_ADMIN_LIST_ZIP_CODE
$l[0]['ville'] = $this->getLang('L_FORM_CITY');//L_ADMIN_LIST_CITY
$l[0]['activitee'] = $this->getLang('L_FORM_ACTIVITY');//L_ADMIN_LIST_ACTIVITY
$l[0]['societe'] = $this->getLang('L_FORM_SOCIETY');//L_ADMIN_LIST_STRUCTURE
$l[0]['service'] = $this->getLang('L_FORM_SERVICE');//L_ADMIN_LIST_DPT
$l[0]['tel2'] = $this->getLang('L_FORM_TEL_OFFICE');//L_ADMIN_LIST_TEL_OFFICE  L_ADMIN_LIST_TEL_PRO
$l[0]['choix'] = $this->getLang('L_ADMIN_LIST_CHOICE');//L_FORM_CHOICE
$l[0]['infolettre'] = $this->getLang('L_ADMIN_LIST_MAILING');
$l[0]['annuaire'] = $this->getLang('L_ADMIN_LIST_COORDONNEES');

$e[]['nom_asso'] = $this->getParam('nom_asso') . ' (' . date($this->getLang('L_DATE_FORMAT').' H:i:s').')';
//============================================================+
//Impression en xlsx ou ods
//============================================================+
if (isset($_GET['print']) && ($_GET['print']== 'xlsx' ||$_GET['print'] == 'ods')) :
$tpl = $_GET['print'];
$TBS = new clsTinyButStrong; // new instance of TBS
$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load OpenTBS plugin
$TBS->NoErr = true;
$suffix = $this->getParam('nom_asso');
$debug = 0;
// Retrieve the template to open
$template = ($tpl == 'xlsx') ? 'tpl_ms_excel.xlsx' : (($tpl == 'xls') ? 'tpl_ms_excel.xml' : 'tpl_oo_spreadsheet.ods');
//$template = 'tpl_ms_excel.xlsx';
//$template = 'tpl_oo_spreadsheet.ods';
$template = basename($template);
$chemin = PLX_PLUGINS.$this->plug['name'].'/opentbs/'.$template;
$x = pathinfo($chemin);
$template_ext = $x['extension'];
if (substr($template,0,4)!=='tpl_') exit($this->getLang('L_WRONG FILE'));
if (!file_exists($chemin)) exit($chemin." : ".$this->getLang('L_FILE UNEXIST'));
// Prepare some data
$i = 0;
// Load the template
$TBS->LoadTemplate($chemin, OPENTBS_ALREADY_UTF8);
// Define the name of the output file
$file_name = $this->getLang('L_FILE_NAME').$this->getParam('nom_asso').'.'.$x['extension'];
// Merge data
$TBS->MergeBlock('l', $l);//lang
$TBS->MergeBlock('e', $e);//nom asso
$TBS->MergeBlock('a', $this->plxRecord_adherents->result);

// specific merges depending to the document
if ($template_ext=='xlsx') {
	$TBS->PlugIn(OPENTBS_SELECT_SHEET, 1);// change the current sheet
} elseif ($template_ext=='xml') {
	$TBS->Show(TBS_EXCEL_DOWNLOAD, $file_name);// Final merge and download file
	exit();
} elseif ($template_ext=='doc') {
	$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);// delete comments
} elseif ($template_ext=='docx') {
	$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);// delete comments
} elseif ($template_ext=='odt') {
	$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);// delete comments
}

// Output as a download file (some automatic fields are merged here)
if ($debug==3) { // debug mode 3
	$TBS->Plugin(OPENTBS_DEBUG_XML_SHOW);
} elseif ($suffix===$this->getParam('nom_asso')) {
	// download
	$file_name = str_replace('tpl_',$suffix.'_',$file_name);
	$TBS->Show(OPENTBS_DOWNLOAD, $file_name);
} else {
	// save as file
	$file_name = str_replace('tpl_',$suffix.'_',$file_name);
	$TBS->Show(OPENTBS_FILE+TBS_EXIT, $file_name);
}
//============================================================+
//Fin Impression en xlsx ou ods
//============================================================+
//============================================================+
//Impression en xls
//============================================================+
elseif (isset($_GET['print']) && ($_GET['print']== 'xls') ) :

$tpl = $_GET['print'];
$suffix = $this->getParam('nom_asso');
$debug = 0;

// Retrieve the template to open
$template = ($tpl == 'xls') ? 'tpl_ms_excel.xml' : '';
$template = basename($template);
$chemin = PLX_PLUGINS.$this->plug['name'].'/opentbs/'.$template;
$x = pathinfo($chemin);
$template_ext = $x['extension'];
if (substr($template,0,4)!=='tpl_') exit($this->getLang('L_WRONG FILE'));
if (!file_exists($chemin)) exit($chemin." : ".$this->getLang('L_FILE UNEXIST'));

// Prepare some data
$i = 0;

// Define the name of the output file
$file_name = $this->getLang('L_FILE_NAME').$this->getParam('nom_asso').'.xls';

$TBS = new clsTinyButStrong;// new instance of TBS

// Install the Excel plug-in (must be before LoadTemplate)
$TBS->PlugIn(TBS_INSTALL, TBS_EXCEL);

// Load the Excel template
$TBS->LoadTemplate($chemin, PLX_CHARSET);//OPENTBS_ALREADY_UTF8 is unsseted

// Merge data
$TBS->MergeBlock('l', $l);//lang
$TBS->MergeBlock('e', $e);//nom asso
$TBS->MergeBlock('a', $this->plxRecord_adherents->result);

// Final merge and download file
$TBS->Show(TBS_EXCEL_DOWNLOAD, $file_name);

//============================================================+
//Fin Impression en xls
//============================================================+
endif;