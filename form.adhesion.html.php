<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS plxShow
 * @version	2.3.3
 * @date	05/08/2020
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/

if(
(isset($_SESSION['lockArticles']['articles'] ,$_SESSION['lockArticles']['categorie'] ,$_SESSION['account']))
&&($_SESSION['lockArticles']['articles'] == 'on' && $_SESSION['lockArticles']['categorie'] == 'on' && $_SESSION['account'] != '')
) {#Si l'utilisateur est connecté, on le redirige vers la page "mon compte" #note : $_SESSION['ValidAd'] ==  '1'
	header('Location:'.$this->plxMotor->urlRewrite('?myaccount.html'));
	exit;
}

$useCapcha = TRUE;
include('form.init.inc.php');#init plug & capcha
#init vars
$id = $plxPlugin->nextIdAdherent();
$pro = array();
$error = array();
$success = false;
$wall_e = '';
if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
 $aA = explode(',',$plxPlugin->getParam('tabActivites'));
 $aK = array_map('strtolower', $aA);
 $aActivites = array_combine($aK, $aA);
 $firstActivityId = key($aActivites);#php 7 : array_key_first
}
if (isset($_GET['q'])) {# Désinscription des listes de diffusion
	$erase = $plxPlugin->compare($_GET['q']);
	if ($erase) {# enregistrée.
		$_SESSION['lockArticles']['success'] .= $plxPlugin->getLang('L_FORM_ERASE_FORM_LIST_OK').'<br />';
		header('location:'.$this->plxMotor->urlRewrite('?login-page.html'));
		exit();
	}
}

#jeton qui valide une addresse mél ou un compte si années illimitées + param :) #tricks with #retrieveMyPass funk
if(isset($_GET['validmail'])) {#v2.2.2
	$info = $plxPlugin->getLang('L_VALIDMAIL_KO');#par défaut Erreur de clé ou de courriel #notif
	$redir = '';#acceuil du site
	$sess = 'error';#notif
	if(isset($_GET['email']) AND !empty($_GET['email'])) {
		foreach(array('email','p','a','z') AS $v){#piece @jeton | @token, adherent id, time
			${$v} = '';#bep
			if(isset($_GET[$v]) AND !empty($_GET[$v])){
				${$v} = $_GET[$v];#bep
			}
		}
		if($p AND $a AND $z){
			if(is_numeric($a)){#aherent id
				$a = str_pad($a, 5, STR_PAD_LEFT);
				$id = $plxPlugin->adherentsList[$a];
				$sel = $plxPlugin->plxRecord_adherents->result[$id]['salt'];
				if($email == sha1($plxPlugin->plxRecord_adherents->result[$id]['salt'].$plxPlugin->plxRecord_adherents->result[$id]['mail'])){#crypté
					$email = $plxPlugin->plxRecord_adherents->result[$id]['mail'];#remplace le mél
				}
			}
		}
		$plxPlugin->retrieveMyPass($email, $a, $p);#ici on verifie le token, le valide et crée $plxPlugin->mailIsValid & $plxPlugin->retrievePass
		if($plxPlugin->mailIsValid AND $plxPlugin->retrievePass){#garde l'info : cle rand1&2 == -1no-1 (a l'inscription)
			#$plxPlugin->retrievePass# autoValid or mailIsValid
			$info = $plxPlugin->getLang('L_VALIDMAIL_OK');#Courriel valide et informe #retour html ou notif
			$sess = 'success';
			if($plxPlugin->getParam('autoValid')){#Courriel valide et autovalid ou $plxPlugin->retrievePass == 'autoValid '
				$info = $plxPlugin->getLang('L_VALIDMAIL_AND_ACCOUNT_OK');
				$redir = '?login-page.html';
			}
		}
	}
	$_SESSION['lockArticles'][$sess] .= $info.'<br />';#notif
	header('location:'.$this->plxMotor->urlRewrite($redir));#on redirige vers login-page.html
	exit;
}
if(!empty($_POST) && !empty($_POST['wall-e'])) {
	$wall_e = $_POST['wall-e'];
}
if(!empty($_POST) && empty($_POST['wall-e']) && isset($_POST['nom_'.$id])) {
	$nom=strtolower(trim(plxUtils::strCheck($_POST['nom_'.$id])));
	$prenom=strtolower(trim(plxUtils::strCheck($_POST['prenom_'.$id])));

	if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
		$activite=isset($_POST['activite_'.$id])?trim(plxUtils::strCheck($_POST['activite_'.$id])):'';
		$activite_autre=trim(plxUtils::strCheck($_POST['activite_autre_'.$id]));
		$etablissement=trim(plxUtils::strCheck($_POST['etablissement_'.$id]));
		$service=trim(plxUtils::strCheck($_POST['service_'.$id]));
		$tel_office=str_replace(array('.','-',' ','_','+','(',')',',',':',';','/'),'',plxUtils::strCheck($_POST['tel_office_'.$id]));
		if($activite =='')
			$error[] = $plxPlugin->getLang('L_ERR_ACTIVITE');
		if($activite =='autre' && trim($activite_autre) == '')
			$error[] = $plxPlugin->getLang('L_ERR_AUTRE_ACTIVITE');
		if(trim($etablissement) == '')
			$error[] = $plxPlugin->getLang('L_ERR_ETABLISSEMENT');
		if(trim($service) == '')
			$error[] = $plxPlugin->getLang('L_ERR_SERVICE');
		if(trim($tel_office) != '' && !preg_match('!^[0-9]{9,13}[0-9]$!',$tel_office))
			$error[] = $plxPlugin->getLang('L_ERR_TEL_OFFICE');
		$pro = array(
			'activite'=>$activite,
			'activite_autre'=>$activite_autre,
			'etablissement'=>$etablissement,
			'service'=>$service,
			'tel_office'=>$tel_office,
			'coordonnees'=>''
		);
	}

	if($plxPlugin->getParam('showAnnuaire') == 'on') {
		$coordonnees = isset($_POST['coordonnees_'.$id])?trim(plxUtils::strCheck($_POST['coordonnees_'.$id])):'';
		if(trim($coordonnees) == '')
			$error[] = $plxPlugin->getLang('L_ERR_COORDONNEES');
		else
			$pro['coordonnees'] = $coordonnees;
	}

	$adresse1=trim(plxUtils::strCheck($_POST['adresse1_'.$id]));
	$adresse2=trim(plxUtils::strCheck($_POST['adresse2_'.$id]));
	$cp=$_POST['cp_'.$id];
	$ville=trim(plxUtils::strCheck($_POST['ville_'.$id]));
	$tel=str_replace(array('.','-',' ','_','+','(',')',',',':',';','/'),'',$_POST['tel_'.$id]);
	$mail=trim(str_replace('&#64;', '@', plxUtils::strCheck($_POST['mail_'.$id])));
	$mailR=trim(str_replace('&#64;', '@', plxUtils::strCheck($_POST['mailR_'.$id])));
	$choix=isset($_POST['choix_'.$id])?plxUtils::strCheck($_POST['choix_'.$id]):'';
	$mailing=isset($_POST['mailing_'.$id])?plxUtils::strCheck($_POST['mailing_'.$id]):'';
	if(trim($nom)=='')
		$error[] = $plxPlugin->getLang('L_ERR_NAME');
	if(trim($prenom)=='')
		$error[] = $plxPlugin->getLang('L_ERR_FIRST_NAME');
	if(trim($adresse1) == '')
		$error[] = $plxPlugin->getLang('L_ERR_ADRESSE');
	if(trim($cp) == '' || strlen($cp) != 5 || !is_numeric($cp))
		$error[] = $plxPlugin->getLang('L_ERR_CP');
	if(trim($ville) == '')
		$error[] = $plxPlugin->getLang('L_ERR_VILLE');
	if(trim($tel) == '' || !preg_match('!^[0-9]{9,13}[0-9]$!',$tel))
		$error[] = $plxPlugin->getLang('L_ERR_TEL');
	if(trim($choix) == '')
		$error[] = $plxPlugin->getLang('L_ERR_CHOIX');
	if(trim($mailing) == '')
		$error[] = $plxPlugin->getLang('L_ERR_MAILING');
	if(!plxUtils::checkMail($mail))
		$error[] = $plxPlugin->getLang('L_ERR_MAIL');
	if(!plxUtils::checkMail($mailR))
		$error[] = $plxPlugin->getLang('L_ERR_MAILR');#Retype
	if($mail != $mailR)
		$error[] = $plxPlugin->getLang('L_ERR_MAILS');#Sames?
	if($this->plxMotor->aConf['capcha']) {#capcha activé
		# Compatibilité avec les plugins de captcha
		if(isset($this->plxMotor->plxPlugins->aPlugins['LionCaptcha'])) {# LionCaptcha 2.0
			if($this->plxMotor->plxPlugins->aPlugins['LionCaptcha']->capcha->actionBegin() !== true) {# Réponse érroné
				$error[] = $plxPlugin->getLang('L_ERR_ANTISPAM');
			}
		}else{#classique
			if (isset($this->plxMotor->plxPlugins->aPlugins['plxCapchaImage'])) {# plxMyCapchaImage
				$_SESSION['capcha']=sha1(@$_SESSION['capcha']);
			}
			if(!isset($_POST['rep']) OR (empty($_POST['rep']) OR $_SESSION['capcha'] != sha1($_POST['rep']))) {# capcha originel
				$error[] = $plxPlugin->getLang('L_ERR_ANTISPAM');
			}
		}
	}#fi capcha activé

	foreach ($plxPlugin->adherentsList as $adherent) {
		if ($nom.' '.$prenom == $adherent['nom'].' '.$adherent['prenom'] && $mail == $adherent['mail'] && $adherent['validation'] == '1' && $choix == 'adhesion') {
			$error['extra'] = $plxPlugin->getLang('L_ERR_USER_ALREADY_USED');
		}
	}
	if(empty($error)) {
		$content = $plxPlugin->notification($nom,$prenom,$adresse1,$adresse2,$cp,$ville,$tel,$mail,$choix,$mailing,$pro);#RETOUR / MÉL HTML pour l'admin
		# On édite (ajoute a) la liste des adhérents
		$edit = $plxPlugin->editAdherentslist($_POST,$id);

		if ($choix != 'stop') {
			$logInBase = str_replace(array('-','_'),'',plxUtils::title2url(strtolower($nom.$prenom)));#login
			#Si pas d'erreur, envoie du mail à l'admin contenant les informations de l'adhérent
			if($plxPlugin->sendEmail($plxPlugin->getParam('nom_asso'),$plxPlugin->getParam('email'),$plxPlugin->getParam('email'),$plxPlugin->getParam('subject'),$content,'html')){
				if ($choix == 'renouveler') {
					#Envoie du mail à l'adhérent
					if($mail = $plxPlugin->sendEmail($plxPlugin->getParam('nom_asso'),$plxPlugin->getParam('email'),$mail,$plxPlugin->getParam('subject'),'<p>'.$plxPlugin->getLang('L_BJR_MSG').' '.$prenom.' '.$nom.'.<br/>'.$plxPlugin->getParam('thankyou').'</p>'.$plxPlugin->adresse(),'html')) {
						if (empty($error)){
							$success = $plxPlugin->getParam('thankyou');#RETOUR HTML
						}
					}
				} elseif (empty($error)){
					$success = $plxPlugin->getParam('thankyou');#RETOUR HTML
				}
			}
			if(!$mail)#on informe si une erreur d'envoi, affiches les infos, sans le "verifier votre spam box" ;)
				$info = '<b class="contact_error">'.$plxPlugin->getLang('L_ERR_SENDEMAIL').'!</b><br/><br/>'.$plxPlugin->getLang('L_BJR_MSG').$prenom.' '.$nom.'.<br/><br/>'.$plxPlugin->getParam('thankyou').'<br/><br/>'.$plxPlugin->getLang('L_ADMIN_ID').'&nbsp;: <b><u>'.$logInBase.'</u></b><br/><br/>'.$plxPlugin->adresse(FALSE);
		} elseif($choix == 'stop') {
			$info = $plxPlugin->getLang('L_FORM_ERASE');
		}
	}
}
else {
	$nom =
	$prenom =
	$adresse1 =
	$adresse2 =
	$cp =
	$ville =
	$tel =
	$mail =
	$mailR = '';
	$choix = '';#adhesion renouveler stop
	$mailing = '';#maillist blacklist
	if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
		$activite = $firstActivityId;
		$activite_autre = '';
		$etablissement = '';
		$service = '';
		$tel_office = '';
	}
	if($plxPlugin->getParam('showAnnuaire') == 'on') {
		$coordonnees='';# public rec refus
	}
}
?>
<div id="form_adherer">
<?php
$_POST = '';
if(!empty($info)) {
?>
	<div class="contact_success"><?= $info ?></div>
<?php
	$nom =
	$prenom =
	$adresse1 =
	$adresse2 =
	$cp =
	$ville =
	$tel =
	$mail =
	$mailR = '';
	$choix = '';#adhesion renouveler stop
	$mailing = '';#maillist blacklist
	if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
		$activite = $firstActivityId;
		$activite_autre = '';
		$etablissement = '';
		$service = '';
		$tel_office = '';
	}
	if($plxPlugin->getParam('showAnnuaire') == 'on') {
		$coordonnees='';#rec public refus
	}
}#!empty($info)
else {#1st time | #Errors
	if(!empty($error)):# 0 ?>
		<div class="contact_error">
<?php if(isset($error['extra'])) :
				echo '<p>'.$error['extra'].'</p>';
				unset($error['extra']);
				endif;#$error['extra']
				if(!empty($error)):# 1
?>
				<h3><?php $plxPlugin->lang('L_FORM_FIELDS_MISSING') ?></h3>
				<ul>
<?php foreach ($error as $e) {
					echo PHP_EOL.'						<li>'.$e.'</li>';
				}
?>
				</ul>
<?php endif;#!empty($error) 1 ?>
		</div>
<?php endif;#!empty($error) 0 ?>
<?php unset($_POST);
		if($success):
			$_POST = '';
#			echo $plxPlugin->getLang('L_ADMIN_PASSWORD').'&nbsp;: '.$l['cle'].'-'.substr($l['mail'],0,-$l['rand1']).$l['rand2'];#v <= 2.2.1
#			echo $plxPlugin->getLang('L_ADMIN_PASSWORD').'&nbsp;: '.$l['cle'].'-'.$l['rand1'].$l['rand2'];#v2.2.2
?>
		<p id="showpass"><?= $plxPlugin->getLang('L_ADMIN_ID'); ?>&nbsp;: <b><u><?= $logInBase ; ?></u></b></p>
<?php
			#On affiche les instructions pour finaliser l'adhésion
			echo $plxPlugin->adresse();
		else:
?>
	<p id="all_required"><?= sprintf($plxPlugin->getLang('L_FORM_ALL_REQUIRED'),'<exp class="mandatory">*</exp>');?></p>
	<form action="#form" method="post" name="monadhesion">
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_IDENTITY');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
		<p>
			<label for="nom"><?php $plxPlugin->lang('L_FORM_NAME') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="nom" name="nom_<?= $id?>" type="text" size="30" pattern="[^0-9]+" value="<?= plxUtils::strCheck($nom) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
		<p>
			<label for="prenom"><?php $plxPlugin->lang('L_FORM_FIRST_NAME') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="prenom" name="prenom_<?= $id?>" type="text" size="30" pattern="[^0-9]+" value="<?= plxUtils::strCheck($prenom) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
		</fieldset>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') :
#
 $tot=count($aActivites);
 $ia=0;
?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_ACTIVITY');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
<?php foreach($aActivites AS $iActi => $acti): echo ++$ia==$tot?'<br/>':''; ?>
		<p class="act">
			<input id="<?= $iActi ?>" name="activite_<?= $id?>" type="radio" value="<?= $iActi ?>" <?= plxUtils::strCheck($activite) == $iActi? 'checked="checked"' : ''; ?> required />
			<label for="<?= $iActi ?>"><?= $acti ?><exp class="mandatory">*</exp></label>
		</p>
<?php endforeach; ?>
		<p>
			<label class="mask" for="activite_autre"><?= $plxPlugin->lang('L_FORM_DETAIL');?>&nbsp;:</label>
			<input class="mask" id="activite_autre" name="activite_autre_<?= $id?>" type="text" value="<?= plxUtils::strCheck($activite_autre);?>" />
		</p>
		</fieldset>
<?php endif;#typeAnnuaire == professionnel ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_AGENDA');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
		<p>
			<label for="etablissement"><?php $plxPlugin->lang('L_FORM_SOCIETY') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="etablissement" name="etablissement_<?= $id?>" type="text" size="50" value="<?= plxUtils::strCheck($etablissement) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
		<p>
			<label for="service"><?php $plxPlugin->lang('L_FORM_SERVICE') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="service" name="service_<?= $id?>" type="text" size="50" value="<?= plxUtils::strCheck($service) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
<?php endif;#typeAnnuaire == professionnel ?>
		<p>
			<label for="adresse1"><?php $plxPlugin->lang('L_FORM_ADDRESS') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input class="xl" id="adresse1" name="adresse1_<?= $id?>" type="text" size="50" value="<?= plxUtils::strCheck($adresse1) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
			<input class="xl" id="adresse2" name="adresse2_<?= $id?>" type="text" size="50" value="<?= plxUtils::strCheck($adresse2) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_OPTIONAL'); ?>" maxlength="50" />
		</p>
		<p>
			<label for="cp"><?php $plxPlugin->lang('L_FORM_ZIP_CODE') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="cp" name="cp_<?= $id?>" type="text" size="16" value="<?= plxUtils::strCheck($cp) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="5" required />
		</p>
		<p>
			<label for="ville"><?php $plxPlugin->lang('L_FORM_CITY') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="ville" name="ville_<?= $id?>" type="text" size="50" value="<?= plxUtils::strCheck($ville) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
		<p>
			<label for="telnum"><?php $plxPlugin->lang('L_FORM_TEL') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="telnum" name="tel_<?= $id?>" type="text" size="50" value="<?= plxUtils::strCheck($tel) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
		<p>
			<label for="tel_office"><?php $plxPlugin->lang('L_FORM_TEL_OFFICE') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="tel_office" name="tel_office_<?= $id?>" type="text" size="50" value="<?= plxUtils::strCheck($tel_office) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
<?php endif;#typeAnnuaire == professionnel ?>
		<p>
			<label for="courriel"><?php $plxPlugin->lang('L_FORM_MAIL') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="courriel" name="mail_<?= $id?>" type="email" size="255" value="<?= ($mail != '')? str_replace('@','&#64;',$mail):''; ?>" required placeholder="<?php $plxPlugin->lang('L_FORM_MAIL_PH') ?> <?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" oninput="mailSame(this)" />
		</p>
		<p>
			<label for="courrielR"><?php $plxPlugin->lang('L_FORM_MAILR') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="courrielR" name="mailR_<?= $id?>" type="email" size="255" value="<?= ($mailR != '')? str_replace('@','&#64;',$mailR):''; ?>" required placeholder="<?php $plxPlugin->lang('L_FORM_MAILR_PH') ?> <?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" oninput="mailSame(this)" />
		</p>
		</fieldset>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_CHOICE');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
		<p>
			<input id="adhesion" name="choix_<?= $id?>" type="radio" value="adhesion" <?= plxUtils::strCheck($choix) == 'adhesion'? 'checked="checked"' : ''; ?> required />
			<label for="adhesion"><?php $plxPlugin->lang('L_FORM_RULES') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php if($plxPlugin->getParam('annee') != 'illimite'): ?>
		<p>
			<input id="renouveler" name="choix_<?= $id?>" type="radio" value="renouveler" <?= plxUtils::strCheck($choix) == 'renouveler'? 'checked="checked"' : ''; ?> required />
			<label for="renouveler"><?php $plxPlugin->lang('L_FORM_RULES_RE') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="stop" name="choix_<?= $id?>" type="radio" value="stop" <?= plxUtils::strCheck($choix) == 'stop'? 'checked="checked"' : ''; ?> required />
			<label for="stop"><?php $plxPlugin->lang('L_FORM_RULES_NO') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php endif;#$plxPlugin->getParam('annee') != 'illimite') ?>
		</fieldset>
<?php if($plxPlugin->getParam('showAnnuaire') == 'on') : ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_SHARING');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
<?php if($plxPlugin->getParam('publicAnnuaire') == 'on') : ?>
		<p>
			<input id="public" name="coordonnees_<?= $id?>" type="radio" value="public" <?= plxUtils::strCheck($coordonnees) == 'public' ? 'checked="checked"' : ''; ?> required />
			<label for="public"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_PUBLIC') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php endif;#($plxPlugin->getParam('publicAnnuaire') == 'on') ?>
		<p>
			<input id="rec" name="coordonnees_<?= $id?>" type="radio" value="rec" <?= plxUtils::strCheck($coordonnees) == 'rec' ? 'checked="checked"' : ''; ?> required />
			<label for="rec"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_MEMBER') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="refus" name="coordonnees_<?= $id?>" type="radio" value="refus" <?= plxUtils::strCheck($coordonnees) == 'refus' ? 'checked="checked"' : ''; ?> required />
			<label for="refus"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_NO') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		</fieldset>
<?php else:# ($plxPlugin->getParam('showAnnuaire') == 'on') ?>
			<input id="annuoff" name="coordonnees" type="hidden" value="refus" />
<?php endif;#fi ($plxPlugin->getParam('showAnnuaire') == 'on') ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_MAILING');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
		<p>
			<input id="maillist" name="mailing_<?= $id?>" type="radio" value="maillist" <?= plxUtils::strCheck($mailing) == 'maillist' ? 'checked="checked"' : ''; ?> required />
			<label for="maillist"><?php $plxPlugin->lang('L_FORM_RULES_NEWS') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="blacklist" name="mailing_<?= $id?>" type="radio" value="blacklist" <?= plxUtils::strCheck($mailing) == 'blacklist' ? 'checked="checked"' : ''; ?> required />
			<label for="blacklist"><?php $plxPlugin->lang('L_FORM_RULES_NEWS_NO') ?></label>
		</p>
		<p class="wall-e">
			<label for="walle"><?php $plxPlugin->lang('L_FORM_WALLE') ?>&nbsp;:</label>
			<input id="walle" name="wall-e" type="text" size="50" value="<?= plxUtils::strCheck($wall_e) ?>" maxlength="50" />
		</p>
		</fieldset>
		<fieldset>
<?php if($this->plxMotor->aConf['capcha']): #$this->lang('ANTISPAM_WARNING')?>
			<p>
				<label for="id_rep"><strong><?php $plxPlugin->lang('L_FORM_ANTISPAM') ?><exp class="mandatory">*</exp>&nbsp;:</strong></label>
			</p>
			<?php $this->capchaQ(); ?>
			<input id="id_rep" name="rep" type="text" size="2" maxlength="1" autocomplete="off" style="width: auto; display: inline;" required placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" />
<?php endif;#capcha ?>
		<p class="text-right">
			<input type="submit" name="submit" value="<?php $plxPlugin->lang('L_FORM_BTN_SEND') ?>" />
		</p>
		</fieldset>
	</form>
	<script type="text/javascript">
/* <![CDATA[ */
		function mailSame() {
			var courriel = document.getElementById('courriel');
			var courrielR = document.getElementById('courrielR');
			if (courriel.value != courrielR.value) {
				courriel.setCustomValidity('<?php $plxPlugin->lang('L_FORM_JS_MAILS_MISMATCH') ?>');
				courrielR.setCustomValidity('<?php $plxPlugin->lang('L_FORM_JS_MAILS_MISMATCH') ?>');
			} else {// inputs is valid -- reset the error message
				courriel.setCustomValidity('');
				courrielR.setCustomValidity('');
			}
		}
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
		var rad = document.monadhesion.activite_<?= $id?>;
		var othr = document.getElementsByClassName('mask');
		for(var m = 0;m < othr.length; m++)
			othr[m].style.display='none';
		for(var i = 0; i < rad.length; i++){
			rad[i].onclick = function() {
				if(this.value == 'autre'){//on affiche
					for(var m = 0;m < othr.length; m++)
						othr[m].style.display='';
					document.getElementById('activite_autre').required = true;
					document.getElementById('activite_autre').focus();
				}else{//on cache
					for(var m = 0;m < othr.length; m++)
						othr[m].style.display='none';
					document.getElementById('activite_autre').required = false;
					document.getElementById('activite_autre').value = '';
				}
			};
		}
<?php endif;#typeAnnuaire == professionnel ?>
/* ]]> */
	</script>

<?php endif;#$success (ELSE)
}#fi else !empty($info), #1st time | #errors ?>
</div>
