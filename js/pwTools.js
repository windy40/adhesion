function pwAStrength(id, s) {//inspiré de pwdStrength() du fichier lib/visual.js de pluxml 5.7
	// Colors: white = empty, red = very weak, orange = weak, yellow = good, green = strong
	var color = ['#fff', '#ff0000', '#ff9900', '#ffcc00', '#33cc33'];
	var val = document.getElementById(id).value;
	var no=0;
	// If the password length is less than or equal to 6
	if(val.length>0 && val.length<=8) no=1;
	// If the password length is greater than 8 and contain any lowercase alphabet or any number or any special character
	if(val.length>8 && (val.match(/[a-z]/) || val.match(/\d+/) || val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))) no=2;
	// If the password length is greater than 8 and contain alphabet,number,special character respectively
	if(val.length>8 && ((val.match(/[a-z]/) && val.match(/\d+/)) || (val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) || (val.match(/[a-z]/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)))) no=3;
	// If the password length is greater than 8 and must contain alphabets,numbers and special characters
	if(val.length>8 && val.match(/[a-z]/) && val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) no=4;
	// If the password length is greater than 13 characters and must contain alphabets (long sentence) my dog is marvelous
	if(val.length>13 && val.match(/[a-z]/)) no=4;
	// Change password background color
	document.getElementById(id).style.backgroundColor=color[no];
	// Change label strenght password
	var pwdstr=document.getElementById(id+'_strenght');
	pwdstr.innerHTML='';if(no>0){pwdstr.innerHTML=s[no-1]};
}